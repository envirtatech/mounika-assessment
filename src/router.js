const express = require('express');
const controller = require('./controller');
const router = express.Router();

router.get('/alphaNumbers/:number', async (req, res) => {
    const { number } = req.params;
    const { limit, offset } = req.query;
    try {
      const result = await controller.convertToAlphaNumeric(number, Number(limit), Number(offset));
      res.send({
        success: true,
        data: result,
      });
    } catch (err) {
      res.status(400).send(err.message);
    }
});

module.exports = router;
