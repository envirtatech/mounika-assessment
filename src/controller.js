const services = require('./services');

const controller = {
  async convertToAlphaNumeric(phoneNumber, limit = 50, offset = 0) {
    try {
      if (!phoneNumber || isNaN(Number(phoneNumber)) || phoneNumber.length < 7 || phoneNumber.length > 10
        || (phoneNumber.length > 7 && phoneNumber.length < 10)) {
        throw new Error('Invalid PhoneNumber');
      }
      const numbers = await services.convertPhoneNumber(phoneNumber);
      const results = numbers.slice(offset, offset + limit);
      return {
        total: numbers.length,
        results,
        limit,
        offset,
      };
    } catch (error) {
      throw error;
    }
  }
};

module.exports = controller;